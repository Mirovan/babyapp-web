package ru.bigint.babyapp.front.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.RoleDAO;
import ru.bigint.babyapp.front.data.dao.UserConfigDAO;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.data.model.entities.UserConfig;
import ru.bigint.babyapp.front.service.iface.UserConfigService;

@Service("userConfigService")
public class UserConfigImpl implements UserConfigService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("userConfigDAO")
    private UserConfigDAO userConfigDAO;

    @Override
    public void update(UserConfig userConfig) {
        userConfigDAO.save(userConfig);
    }

    @Override
    public UserConfig findByUser(User user) {
        return userConfigDAO.findByUser(user);
    }
}
