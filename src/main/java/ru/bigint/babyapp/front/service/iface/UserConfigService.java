package ru.bigint.babyapp.front.service.iface;

import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.data.model.entities.UserConfig;

public interface UserConfigService {
    void update(UserConfig userConfig);
    UserConfig findByUser(User user);
}
