package ru.bigint.babyapp.front.service.iface;


import ru.bigint.babyapp.front.data.model.entities.Role;

import java.util.List;
import java.util.Set;

public interface RoleService {
    Role findRoleByName(String roleName);
}
