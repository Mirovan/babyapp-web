package ru.bigint.babyapp.front.service.iface;

import ru.bigint.babyapp.front.data.model.adapters.PublicUser;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.data.model.entities.User;

import java.util.Set;

public interface UserService {
    void addUser(User user, Set<Role> roles);
    void update(User user, User updateUser);
    User findUserByUID(String id);
    User findUserByEmail(String email);
    User findUserByPhone(String prone);
    String generateRandomPassword();            //Генерация случайного пароля
    String encodePassword(String password);     //Шифрование пароля
    PublicUser getPublicUser(User user);        //Получение публичного пользователя
}
