package ru.bigint.babyapp.front.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.RoleDAO;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.service.iface.RoleService;

import java.util.List;
import java.util.Set;

@Service("roleService")
public class RoleServiceImpl implements RoleService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("roleDAO")
    private RoleDAO roleDAO;

    @Override
    public Role findRoleByName(String roleName) {
        return roleDAO.findByRole(roleName);
    }
}
