package ru.bigint.babyapp.front.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bigint.babyapp.front.data.dao.RoleDAO;
import ru.bigint.babyapp.front.data.dao.UserDAO;
import ru.bigint.babyapp.front.data.model.adapters.PublicUser;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.service.iface.RoleService;
import ru.bigint.babyapp.front.service.iface.UserService;

import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("userDAO")
    private UserDAO userDAO;

    @Autowired
    @Qualifier("roleDAO")
    private RoleDAO roleDAO;

    @Autowired
    private RoleService roleService;


    @Override
    public void addUser(User user, Set<Role> roles) {
        user.setRoles(roles);
        userDAO.save(user);
    }


    @Override
    public void update(User user, User updateUser) {
        user.setEmail( updateUser.getEmail() );
        user.setPhone( updateUser.getPhone() );
        user.setName( updateUser.getName() );
        user.setSirname( updateUser.getSirname() );
        user.setLastname( updateUser.getLastname() );
        if (updateUser.getPass() != null && !updateUser.getPass().trim().equals(""))
            user.setPass( encodePassword(updateUser.getPass()) );

        userDAO.save(user);
    }


    @Override
    @Transactional
    public User findUserByUID(String id) {
        User user = userDAO.findById(Integer.valueOf(id)).get();
        if (user != null) {
            int size = user.getRoles().size();  //Для LAZY hibernate initialization
        }
        return user;
    }


    @Override
    @Transactional
    public User findUserByEmail(String email) {
        User user = userDAO.findByEmail(email);
        if (user != null) {
            int size = user.getRoles().size();  //Для LAZY hibernate initialization
        }
        return user;
    }


    @Override
    @Transactional
    public User findUserByPhone(String phone) {
        User user = userDAO.findByPhone(phone);
        if (user != null) {
            int size = user.getRoles().size();  //Для LAZY hibernate initialization
        }
        return user;
    }


    @Override
    public String generateRandomPassword() {
        return String.valueOf(ThreadLocalRandom.current().nextInt(100000, 999999));
    }

    @Override
    public String encodePassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(password);
    }


    @Override
    public PublicUser getPublicUser(User user) {
        PublicUser publicUser = new PublicUser();
        BeanUtils.copyProperties(user, publicUser);
        return publicUser;
    }
}
