package ru.bigint.babyapp.front.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.AccountTypeDAO;
import ru.bigint.babyapp.front.data.model.entities.AccountType;
import ru.bigint.babyapp.front.service.iface.AccountTypeService;

import java.util.List;

@Service("AccountTypeService")
public class AccountTypeImpl implements AccountTypeService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("accountTypeDAO")
    private AccountTypeDAO accountTypeDAO;

    @Override
    public List<AccountType> findAll() {
        return accountTypeDAO.findAll();
    }

    @Override
    public AccountType findByType(String type) {
        return accountTypeDAO.findByType(type);
    }
}
