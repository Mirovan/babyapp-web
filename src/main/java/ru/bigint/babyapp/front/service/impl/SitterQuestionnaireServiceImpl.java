package ru.bigint.babyapp.front.service.impl;

import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.SitterQuestionnaireDAO;
import ru.bigint.babyapp.front.data.model.entities.QAccountType;
import ru.bigint.babyapp.front.data.model.entities.QSitterQuestionnaire;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.service.iface.SitterQuestionnaireService;

import java.time.LocalDate;
import java.util.List;

@Service("sitterQuestionnaireService")
public class SitterQuestionnaireServiceImpl implements SitterQuestionnaireService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("sitterQuestionnaireDAO")
    private SitterQuestionnaireDAO sitterQuestionnaireDAO;

    @Override
    public void add(SitterQuestionnaire sitterQuestionnaire) {
        sitterQuestionnaireDAO.save(sitterQuestionnaire);
    }

    @Override
    public void update(SitterQuestionnaire sitterQuestionnaire) {
        sitterQuestionnaireDAO.save(sitterQuestionnaire);
    }

    @Override
    public SitterQuestionnaire findByUser(User user) {
        return sitterQuestionnaireDAO.findByUser(user);
    }

    @Override
    public List<SitterQuestionnaire> findAll() {
        return sitterQuestionnaireDAO.findAll();
    }

    @Override
    public Page<SitterQuestionnaire> findByParam(
            String city,
            Integer around,
            Integer gender,
            Integer fromAge,
            Integer toAge,
            List<Integer> childCategories,
            Pageable pageable) {
        //Выбираем только активные анкеты у которых типаккаунта - бебиситтер
        BooleanExpression pred = QSitterQuestionnaire.sitterQuestionnaire.isActive.eq(true);
        //ToDo: сделать выборку учитывая тип аккаунта
        //pred.and(QAccountType.accountType.type.like("BABYSITTER"));

        //Поиск по городу
        if (city != null && city != "") {
            BooleanExpression cityPred = QSitterQuestionnaire.sitterQuestionnaire.city.likeIgnoreCase(city);
            pred = pred.and(cityPred);
        }

        //Поиск по полу
        if (gender != null) {
            BooleanExpression cityPred = QSitterQuestionnaire.sitterQuestionnaire.gender.eq(gender);
            pred = pred.and(cityPred);
        }

        //Нормализуем поиск по возрасту
        if (fromAge != null || toAge != null) {
            if (fromAge == null) fromAge = 0;
            if (toAge == null) toAge = 100;
            //swap age dates
            if (fromAge > toAge) {
                Integer temp = fromAge;
                fromAge = toAge;
                toAge = temp;
            }
            LocalDate fromAgeDate = LocalDate.now().minusYears(toAge);
            LocalDate toAgeDate = LocalDate.now().minusYears(fromAge);
            BooleanExpression agePred = QSitterQuestionnaire.sitterQuestionnaire.birthday.between(fromAgeDate, toAgeDate);
            pred = pred.and(agePred);
        }

        //Поиск по расстоянию - удаленности адреса
        if (around != null) {
            BooleanExpression aroundPred = QSitterQuestionnaire.sitterQuestionnaire.around.between(0, around);
            pred = pred.and(aroundPred);
        }

        //Поиск по категориям детей
            if (childCategories != null && childCategories.size() > 0) {
            BooleanExpression childCategoryPred = QSitterQuestionnaire.sitterQuestionnaire.childCategories.any().id.in(childCategories);
            pred = pred.and(childCategoryPred);
        }

        //return Lists.newArrayList( sitterQuestionnaireDAO.findAll(pred) );
        return sitterQuestionnaireDAO.findAll(pred, pageable);
    }

}
