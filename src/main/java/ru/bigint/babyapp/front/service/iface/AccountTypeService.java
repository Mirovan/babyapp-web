package ru.bigint.babyapp.front.service.iface;

import ru.bigint.babyapp.front.data.model.entities.AccountType;

import java.util.List;

public interface AccountTypeService {
    List<AccountType> findAll();
    AccountType findByType(String type);
}
