package ru.bigint.babyapp.front.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.RoleDAO;
import ru.bigint.babyapp.front.data.dao.UserDAO;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.service.impl.UserServiceImpl;


import java.util.HashSet;
import java.util.Set;

@Service("userDetailService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserServiceImpl userService;

    @Override
    public UserDetails loadUserByUsername(String authId) throws UsernameNotFoundException {
        User user = userService.findUserByEmail(authId);
        if (user == null) {
            user = userService.findUserByPhone(authId);
        }

        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        } else {
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            for (Role role : user.getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
            }
            return new org.springframework.security.core.userdetails.User(
                    user.getId().toString(),
                    user.getPass(),
                    grantedAuthorities
            );
        }
    }

}
