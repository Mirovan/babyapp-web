package ru.bigint.babyapp.front.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.babyapp.front.data.dao.ChildCategoryDAO;
import ru.bigint.babyapp.front.data.model.entities.ChildCategory;
import ru.bigint.babyapp.front.service.iface.ChildCategoryService;

import java.util.List;

@Service("childCategoryService")
public class ChildCategoryServiceImpl implements ChildCategoryService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("childCategoryDAO")
    private ChildCategoryDAO childCategoryDAO;


    @Override
    public List<ChildCategory> fildAll() {
        return childCategoryDAO.findAll();
    }
}
