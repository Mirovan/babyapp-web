package ru.bigint.babyapp.front.service.iface;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.data.model.entities.User;

import java.util.List;

public interface SitterQuestionnaireService {
    void add(SitterQuestionnaire sitterQuestionnaire);
    void update(SitterQuestionnaire sitterQuestionnaire);
    SitterQuestionnaire findByUser(User user);
    List<SitterQuestionnaire> findAll();
    Page<SitterQuestionnaire> findByParam(String city, Integer around, Integer gender, Integer fromAge, Integer toAge, List<Integer> childCategories, Pageable pageable);
}
