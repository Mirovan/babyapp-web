package ru.bigint.babyapp.front.utils;

import java.io.File;
import java.net.URL;

public class Utils {
    public static String getPhotoFileName(Integer userId) {
        URL url = Utils.class.getClassLoader().getResource("static");
        String photosPath = url.getPath() + "/photos/" + userId + ".jpg";
        File file = new File(photosPath);
        String photoName = "nophoto.jpg";
        if (file.exists()) photoName = userId + ".jpg";

        return "/static/photos/" + photoName;
    }
}
