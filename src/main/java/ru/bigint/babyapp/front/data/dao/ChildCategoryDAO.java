package ru.bigint.babyapp.front.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.babyapp.front.data.model.entities.ChildCategory;

import java.util.List;

@Repository("childCategoryDAO")
public interface ChildCategoryDAO extends JpaRepository<ChildCategory, Integer> {
}