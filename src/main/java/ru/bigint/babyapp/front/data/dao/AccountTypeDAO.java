package ru.bigint.babyapp.front.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.babyapp.front.data.model.entities.AccountType;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.data.model.entities.UserConfig;

@Repository("accountTypeDAO")
public interface AccountTypeDAO extends JpaRepository<AccountType, Integer> {
    AccountType findByType(String type);
}