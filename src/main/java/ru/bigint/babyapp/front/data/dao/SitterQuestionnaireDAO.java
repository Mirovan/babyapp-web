package ru.bigint.babyapp.front.data.dao;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.data.model.entities.User;

import java.util.List;

@Repository("sitterQuestionnaireDAO")
public interface SitterQuestionnaireDAO extends
        PagingAndSortingRepository<SitterQuestionnaire, Integer>, QuerydslPredicateExecutor<SitterQuestionnaire> {
    SitterQuestionnaire findByUser(User user);
    List<SitterQuestionnaire> findAll();
    Page<SitterQuestionnaire> findAll(Predicate predicate, Pageable pageable);
}