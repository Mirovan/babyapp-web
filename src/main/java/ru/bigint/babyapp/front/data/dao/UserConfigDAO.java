package ru.bigint.babyapp.front.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.data.model.entities.UserConfig;

@Repository("userConfigDAO")
public interface UserConfigDAO extends JpaRepository<UserConfig, Integer> {
    UserConfig findByUser(User user);
}