package ru.bigint.babyapp.front.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.babyapp.front.data.model.entities.Role;
import ru.bigint.babyapp.front.data.model.entities.User;

import java.util.Set;

@Repository("userDAO")
public interface UserDAO extends JpaRepository<User, Integer> {
    User findByEmail(String email);
    User findByPhone(String phone);
    //Set<User> findByRoles(Role role);
}