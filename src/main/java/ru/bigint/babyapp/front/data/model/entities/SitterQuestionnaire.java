package ru.bigint.babyapp.front.data.model.entities;

import org.springframework.format.annotation.DateTimeFormat;
import ru.bigint.babyapp.front.utils.converter.LocalDateConverter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "sitter_questionnaire")
public class SitterQuestionnaire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "birthday", columnDefinition = "DATE")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @Convert(converter = LocalDateConverter.class)
    private LocalDate birthday;

    @Column(name = "about")
    private String about;

    @Column(name = "city")
    private String city;

    @Column(name = "around")
    private Integer around;

    @Column(name = "edu")
    private String edu;

    @Column(name = "certificates")
    private String certificates;

    @Column(name = "lang")
    private String lang;

    @Column(name = "isSmoke")
    private Boolean isSmoke;

    @Column(name = "religion")
    private String religion;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "isverified")
    private Boolean isVerified;

    @Column(name = "isactive")
    private Boolean isActive;

    @ManyToMany
    @JoinTable(
            name = "sitter_child_categories",
            joinColumns = @JoinColumn(name = "sitter_quest_id"),
            inverseJoinColumns = @JoinColumn(name = "child_category_id")
    )
    private List<ChildCategory> childCategories;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAround() {
        return around;
    }

    public void setAround(Integer around) {
        this.around = around;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Boolean getSmoke() {
        return isSmoke;
    }

    public void setSmoke(Boolean smoke) {
        isSmoke = smoke;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Boolean getVerified() {
        return isVerified;
    }

    public void setVerified(Boolean verified) {
        isVerified = verified;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public List<ChildCategory> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<ChildCategory> childCategories) {
        this.childCategories = childCategories;
    }
}