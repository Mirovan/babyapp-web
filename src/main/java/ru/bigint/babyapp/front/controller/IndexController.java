package ru.bigint.babyapp.front.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;
import ru.bigint.babyapp.front.data.model.entities.ChildCategory;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.service.iface.ChildCategoryService;
import ru.bigint.babyapp.front.service.iface.SitterQuestionnaireService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */

@Controller
public class IndexController/* implements ErrorController*/{

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SitterQuestionnaireService sitterQuestionnaireService;

    @Autowired
    private ChildCategoryService childCategoryService;

    private final int DEFAULT_PAGE_SIZE = 9;


    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView index() {
        List<SitterQuestionnaire> sitterQuestionnaires = sitterQuestionnaireService.findAll();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("sitterQuestionnaires", sitterQuestionnaires);
        modelAndView.setViewName("index");
        return modelAndView;
    }


    /**
     * Страница поиска по параметрам
     * */
    @RequestMapping(value="/search/", method = RequestMethod.GET)
    public ModelAndView searchPage(HttpServletRequest request,
                                   @RequestParam(required = false) String city,
                                   @RequestParam(required = false) Integer around,
                                   @RequestParam(required = false) Integer gender,
                                   @RequestParam(required = false) Integer fromAge,
                                   @RequestParam(required = false) Integer toAge,
                                   @RequestParam(value="childCategories[]", required = false) List<Integer> selectedChildCategories,
                                   @RequestParam(required = false, defaultValue = "0") Integer page,
                                   @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE+"") Integer pageSize) {
        if (selectedChildCategories == null) selectedChildCategories = new ArrayList<>();

        Pageable pageable = PageRequest.of(page, pageSize);
        //Поиск по фильтру
        Page<SitterQuestionnaire> sitterQuestionnaires =
                sitterQuestionnaireService.findByParam(
                        city,
                        around,
                        gender,
                        fromAge,
                        toAge,
                        selectedChildCategories,
                        pageable
                );

        List<ChildCategory> allChildCategories = childCategoryService.fildAll();

        String nextPage = null;
        if (page < sitterQuestionnaires.getTotalPages()-1) {
            nextPage = UriComponentsBuilder.fromHttpRequest(new ServletServerHttpRequest(request))
                    .replaceQueryParam("page", page + 1)
                    .build()
                    .getQuery();
        }

        String prevPage = null;
        if (page > 0) {
            prevPage = UriComponentsBuilder.fromHttpRequest(new ServletServerHttpRequest(request))
                    .replaceQueryParam("page", page - 1)
                    .build()
                    .getQuery();
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("sitterQuestionnaires", sitterQuestionnaires.getContent());
        modelAndView.addObject("allChildCategories", allChildCategories);
        modelAndView.addObject("prevPage", prevPage);
        modelAndView.addObject("nextPage", nextPage);
        modelAndView.addObject("selectedChildCategories", selectedChildCategories);
        modelAndView.setViewName("search/index");
        return modelAndView;
    }

}
