package ru.bigint.babyapp.front.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.bigint.babyapp.front.data.model.adapters.PublicUser;
import ru.bigint.babyapp.front.data.model.entities.*;
import ru.bigint.babyapp.front.service.iface.*;
import ru.bigint.babyapp.front.utils.Utils;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;

@Controller
public class CabinetController {

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private SitterQuestionnaireService sitterQuestionnaireService;

	@Autowired
	private ChildCategoryService childCategoryService;

	@Autowired
	private UserConfigService userConfigService;

	@Autowired
	private AccountTypeService accountTypeService;


	/**
	 * Главная страница личного кабинета
	 * */
	@RequestMapping(value="/cabinet/", method = RequestMethod.GET)
	public ModelAndView index(Authentication authentication) {
		//authentication.getName();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("cabinet/index");
		return modelAndView;
	}


	/**
	 * Страница настроек профиля
	 * */
	@RequestMapping(value="/cabinet/profile/", method = RequestMethod.GET)
	public ModelAndView showProfilePage(Authentication authentication) {
		User user = userService.findUserByUID(authentication.getName());
		PublicUser publicUser = userService.getPublicUser(user);

		String photo = Utils.getPhotoFileName(user.getId());

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("user", publicUser);
		modelAndView.addObject("photo", photo);
		modelAndView.setViewName("cabinet/profile");
		return modelAndView;
	}


	/**
	 * Сохранение настроек профиля - POST
	 * */
	@RequestMapping(value = "/cabinet/profile/", method = RequestMethod.POST)
	public ModelAndView postProfilePage(User updateUser,
										@RequestParam(value = "photoFile", required = false) MultipartFile photoFile,
										Authentication authentication,
										BindingResult bindingResult) {
		User user = userService.findUserByUID( authentication.getName() );

		ModelAndView modelAndView = new ModelAndView();

		//Есть ли пользователь с таким же email ?
		User existsUser = userService.findUserByEmail(updateUser.getEmail());
		if ( existsUser != null && !existsUser.getId().equals(user.getId()) ) {
			bindingResult
					.rejectValue("email", "error.user",
							"There is already a user registered with the email provided");
		}

		//Есть ли пользователь с таким же phone ?
		existsUser = userService.findUserByPhone(updateUser.getPhone());
		if ( existsUser != null && !existsUser.getId().equals(user.getId()) ) {
			bindingResult
					.rejectValue("phone", "error.user", "This phone is already use");
		}

		//Если есть ошибки при смене email или телефона
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("cabinet/profile");
		} else { //изменяем данные пользователя
			userService.update(user, updateUser);
			//Сохраняем фото
			if ( !photoFile.isEmpty() ) {
				try {
					byte[] bytes = photoFile.getBytes();
					URL photosPath = this.getClass().getClassLoader().getResource("static");
					BufferedOutputStream stream =
							new BufferedOutputStream(new FileOutputStream(new File(photosPath.getPath() + "/photos/" + user.getId() + ".jpg")));
					stream.write(bytes);
					stream.close();
				} catch (Exception e) {
					LOGGER.error("Ошибка загрузки файла профиля пользователя");
				}
			}
			return new ModelAndView("redirect:/cabinet/profile/");
		}
		return modelAndView;
	}


	/**
	 * Анкета бебиситтера - форма
	 * */
	@RequestMapping(value="/cabinet/sitter/questionnaire/", method = RequestMethod.GET)
	public ModelAndView showQuestionnairePage(Authentication authentication) {
		User user = userService.findUserByUID(authentication.getName());

		//ищем есть ли уже анкета
		SitterQuestionnaire sitterQuestionnaire = sitterQuestionnaireService.findByUser(user);
		if (sitterQuestionnaire == null) sitterQuestionnaire = new SitterQuestionnaire();

		//выбираем все категории детей
		List<ChildCategory> allChildCategories = childCategoryService.fildAll();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("sitterQuestionnaire", sitterQuestionnaire);
		modelAndView.addObject("allChildCategories", allChildCategories);
		modelAndView.setViewName("cabinet/sitter/questionnaire");
		return modelAndView;
	}


	/**
	 * Анкета бебиситтера - POST
	 * */
	@RequestMapping(value="/cabinet/sitter/questionnaire/", method = RequestMethod.POST)
	public ModelAndView postQuestionnairePage(SitterQuestionnaire sitterQuestionnaire, Authentication authentication) {
		User user = userService.findUserByUID(authentication.getName());

		sitterQuestionnaire.setUser(user);

		//ищем есть ли уже анкета
		if (sitterQuestionnaireService.findByUser(user) == null) {
			sitterQuestionnaireService.add(sitterQuestionnaire);
		} else {
			sitterQuestionnaireService.update(sitterQuestionnaire);
		}

		return new ModelAndView("redirect:/cabinet/sitter/questionnaire/");
	}


	/**
	 * Настройка кабинета (выбор типа аккаунта) - форма
	 * */
	@RequestMapping(value="/cabinet/config/", method = RequestMethod.GET)
	public ModelAndView showConfigPage(Authentication authentication) {
		User user = userService.findUserByUID(authentication.getName());

		UserConfig userConfig = userConfigService.findByUser(user);
		if (userConfig == null) {
			userConfig = new UserConfig();
			userConfig.setUser(user);
		}

		List<AccountType> accountTypes = accountTypeService.findAll();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("userConfig", userConfig);
		modelAndView.addObject("accountTypes", accountTypes);
		modelAndView.setViewName("cabinet/config");
		return modelAndView;
	}


	/**
	 * Настройка кабинета (выбор типа аккаунта) - POST
	 * */
	@RequestMapping(value="/cabinet/config/", method = RequestMethod.POST)
	public ModelAndView postConfigPage(UserConfig userConfig, Authentication authentication) {
		User user = userService.findUserByUID(authentication.getName());

		userConfig.setUser(user);

		//Обновляем
		userConfigService.update(userConfig);

		return new ModelAndView("redirect:/cabinet/config/");
	}


}
