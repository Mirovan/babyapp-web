package ru.bigint.babyapp.front.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;
import ru.bigint.babyapp.front.data.model.entities.ChildCategory;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.data.model.entities.User;
import ru.bigint.babyapp.front.service.iface.AccountTypeService;
import ru.bigint.babyapp.front.service.iface.ChildCategoryService;
import ru.bigint.babyapp.front.service.iface.SitterQuestionnaireService;
import ru.bigint.babyapp.front.service.iface.UserService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */

@Controller
public class UserController/* implements ErrorController*/{

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private SitterQuestionnaireService sitterQuestionnaireService;

    @Autowired
    private ChildCategoryService childCategoryService;

    @Autowired
    private AccountTypeService accountTypeService;


    @RequestMapping(value="/users/{userId}/", method = RequestMethod.GET)
    public ModelAndView showBabySitter(@PathVariable("userId") Integer userId) {
        User user = userService.findUserByUID(userId + "");

        //Если это не бебиситтер то это страницу не открываем
        if (user != null &&
            user.getUserConfig() != null &&
            user.getUserConfig().getAccountType().getId() != accountTypeService.findByType("BABYSITTER").getId())
            return new ModelAndView("redirect:/");

        //Анкета
        SitterQuestionnaire sitterQuestionnairy = sitterQuestionnaireService.findByUser(user);

        //Возраст
        long age = ChronoUnit.YEARS.between(sitterQuestionnairy.getBirthday(), LocalDate.now());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("sitter", sitterQuestionnairy);
        modelAndView.addObject("age", age);
        modelAndView.setViewName("users/index");
        return modelAndView;
    }


}
