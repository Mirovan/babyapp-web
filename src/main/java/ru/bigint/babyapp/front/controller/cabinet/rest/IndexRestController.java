package ru.bigint.babyapp.front.controller.cabinet.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.bigint.babyapp.front.data.model.entities.SitterQuestionnaire;
import ru.bigint.babyapp.front.service.iface.ChildCategoryService;
import ru.bigint.babyapp.front.service.iface.SitterQuestionnaireService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */

@RestController
public class IndexRestController/* implements ErrorController*/{

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final int PAGE_SIZE = 2;

    @Autowired
    private SitterQuestionnaireService sitterQuestionnaireService;

    @Autowired
    private ChildCategoryService childCategoryService;

    /**
     * Страница поиска по параметрам
     * */
    @RequestMapping(value="/search/rest/", method = RequestMethod.GET)
    public List<SitterQuestionnaire> searchPage(HttpServletRequest request,
                                                @RequestParam(required = false) String city,
                                                @RequestParam(required = false) Integer around,
                                                @RequestParam(required = false) Integer gender,
                                                @RequestParam(required = false) Integer fromAge,
                                                @RequestParam(required = false) Integer toAge,
                                                @RequestParam(value="childCategories[]", required = false) List<Integer> selectedChildCategories,
                                                @RequestParam(required = false, defaultValue = "0") Integer page) {
        if (selectedChildCategories == null) selectedChildCategories = new ArrayList<>();

        Pageable pageable = PageRequest.of(page, PAGE_SIZE);
        //Поиск по фильтру
        Page<SitterQuestionnaire> sitterQuestionnaires =
                sitterQuestionnaireService.findByParam(
                        city,
                        around,
                        gender,
                        fromAge,
                        toAge,
                        selectedChildCategories,
                        pageable
                );

        return sitterQuestionnaires.getContent();
    }

}
